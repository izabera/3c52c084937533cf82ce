# only complete shell scripts and options
_shellcheck () {
  local type path tilde="~" REPLY ng whitelist blacklist file IFS=$' \t\n'
  case $2 in
    '~') path=$HOME;;
    '~/'*) path=${2/$tilde/$HOME};;
    *) path=$2;;
  esac

  # parse options
  local inopt=false i end=false
  # stop one word before
  for (( i = 1; i < COMP_CWORD; i++ )); do
    # check if we need an option or a filename
    case ${COMP_WORDS[i]} in
      # only check the options that need extra args
      # ignore options like -eSCxxxx,SCyyyy
      -@(e|f|s)|--@(exclude|format|shell)) inopt=true ;;
      --) end=true; break ;;
      [!-]*) if ! "$inopt"; then
               end=true
               break
             else
               inopt=false
             fi
    esac
  done
  if "$inopt"; then # we're processing an argument to an option
    # we're after an option, completing an empty word
    case $3 in # previous
      # --exclude is not completed
      -f|--format) COMPREPLY=(checkstyle gcc json tty) ;;
      -s|--shell) COMPREPLY=(bash sh ksh)
    esac
    return
  elif ! "$end" && [[ $2 = -* ]]; then # we're processing a new option
    for i in -e -f -s -V --version --exclude= --format= --shell=; do
      [[ $i = "$2"* ]] && COMPREPLY+=("$i") && compopt -o nospace
    done
    return
  fi # end of option parsing

  compopt -o plusdirs
  [[ -d $path && $path != @(.|..) ]] && path=${path%/}/
  local blacklist='@(c|h)?(pp)|tex|pl|md|py|png|jp?(e)g|gif|mp[34]|ogg|tar|gz|xz|bz2|lzma|pdf|a|o|so|wav|flac|mov|flv|avi'
  local whitelist='?(?(b|d)a|?(m)k|z)sh?(rc)'

  shopt -q nullglob; ng=$?; shopt -s nullglob
  COMPREPLY=("$path"*.@($whitelist))

  for file in "$path"!(*.@($whitelist|$blacklist)); do
    [[ -f $file ]] || continue

    # check the shebang line
    read -rn 64 < "$file"
    [[ $REPLY = "#!"*([[:blank:]])@(*/bin/|/usr/bin/env+([[:blank:]]))?(?(b|d)a|?(m)k|z)sh* ]] && COMPREPLY+=("$file")
  done

  if [[ $2 = '~' ]]; then
    local dirs=(~/*/)
    dirs=("${dirs[@]%/}") dirs=("${dirs[@]/#"$HOME"/$tilde}")
    COMPREPLY+=("${dirs[@]}")
  fi
  (( ng )) && shopt -u nullglob
}

complete -F _shellcheck shellcheck